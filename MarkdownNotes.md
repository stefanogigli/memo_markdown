# Basic/Standard Syntax

https://www.markdownguide.org/getting-started/

See MarkdownBasicSyntax.md

## Extensions


- GFM: GitHub Flavored Markdown: https://help.github.com/categories/writing-on-github/
   - provides additional functionality for common formatting needs.
   - https://experienceleague.adobe.com/docs/contributor/contributor-guide/writing-essentials/markdown.html?lang=en

- https://github.com/commonmark/commonmark-spec/wiki/markdown-flavors
Server
## Diagrams

See MarkdownDiagrams0*.md files

- MarkdownDiagrams01.md : Mermaid
- MarkdownDiagrams02.md : Plantuml
- MarkdownDiagrams03.md : ... (dot/dia)

## Maths

See MarkdownMath.md file

## ConverterServer

---

### Visual Studio Code

VisualStudio Code and some plugins ( download: https://code.visualstudio.com/ )

VS Code Plugin

Graphviz (dot) language support for Visual Studio Code - v0.0.6
- auth: João Pinto
- short: This extension provides GraphViz (dot) language support for Visual Studio Code

Markdown All in One - v3.4.3
- auth: Yu Zhang
- short: All you need to write Markdown (keyboard shortcuts, table of contents, auto preview and more)
- Some important sentenced about long description:
    - "Please use Markdown+Math for dedicated math support. Be sure to disable math.enabled option of this extension."
	- "For other Markdown syntax, you need to install the corresponding extensions from VS Code marketplace (eg Mermaid diagram, emoji, footnotes, and superscript) ... "

Markdown Preview Mermaid Support - v1.15.3
- auth: Matt Bierner
- short: Adds Mermaid diagram and flowchart support to VS Code's builtin markdown preview

Markdown Emoji - v0.3.0
- auth: Matt Bierner
- short: Adds ghostwriter
Markdown Footnotes - v0.1.1
- auth: Matt Bierner
- short: Adds [^footnote] syntax support to VS Code's built-in markdown preview

Markdown Superscript - v1.0.6
- auth: DevHawk
- short: Adds ^superscript^ syntax support to VS Code's built-in markdown preview

PlantUML - v2.17.5Server
PlantUML Previewer - v0.6.0
- auth: Mebrahtom Guesh
- short: Plantuml previewer for visual studio code editor


Tra le impostazioni del plugin `plantuml`, non ho capito poerchè, funziona solo se il render è fatto online (occhio quindi a contenuti che si vogliono rendere privati alla compagnia)
 - PlantUML: Render:: select "Server"
 - PlantUML: Server:: set "https://www.plantuml.com/plantuml"

---

### ghostwriter

ghostwriter (https://ghostwriter.kde.org/it/)

- Mac: MacDown, iA Writer, or Marked 2
- iOS / Android: iA Writer
- Windows: ghostwriter or Markdown Monster
- Linux: ReText or ghostwriter
- Web: Dillinger or StackEdit


**NOTE:** `ghostwriter` supporta solo markdown basic, nessuna estensione `mermaid` o altro!


## Doxygen

- https://www.doxygen.nl/manual/markdown.html
- `Input.USE_MDFILE_AS_MAINPAGE` "But INPUT field needs to include that md file. "
- `Project.MARKDOWN_SUPPORT`
- `\include{doc} filename.md` see https://github.com/doxygen/doxygen/issues/7688

In https://www.doxygen.nl/manual/markdown.html, it is explicit which markdown standard and extensions are supported! Actually, `Marmeid` is not supported!


### Altri

- https://geekthis.net/post/markdown-editors/
- remarkable
- typora (ora a pagamento!!!!)
- retext 
- ....

---Server
Riferimenti per sintassi
- https://daringfireball.net/projects/markdown/syntax
    - https://daringfireball.net/projects/markdown/
    - https://daringfireball.net/projects/markdown/basics
    - 
- https://www.markdownguide.org/basic-syntax/
    - https://www.markdownguide.org/hacks/
    - https://www.markdownguide.org/extended-syntax/
- https://docs.github.com/en/get-started/writing-on-github/getting-started-with-writing-and-formatting-on-github/basic-writing-and-formatting-syntax
- https://docs.github.com/en/get-started/writing-on-github/working-with-advanced-formatting/creating-and-highlighting-code-blocks

Generatore di tabelle
- https://www.tablesgenerator.com/markdown_tablesServer

Markdown per scrivere email:
- https://markdown-here.com/


Server