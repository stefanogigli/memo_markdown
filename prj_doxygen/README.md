# Doxygen e Markdown

Questa cartella tenta un esempio di integrazione tra documentazione generata da due tipologie di fonti:

- codice sorgente tra codice e commenti
- file markdown

Siccome `Markdown`, come linguaggio non permette gerarchia tra file e cartelle (non esiste alcun equivalente di `<include>`), si tenta la via in cui un file sorgente include tutti i file markdown presenti.

Buona lettura.
