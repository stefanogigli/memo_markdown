
#include "class_derived_3.h"
#include <iostream>

class_derived_3::class_derived_3(void): class_base()
{
    std::cout << "class_derived_3 CTOR" << std::endl;
}

class_derived_3::~class_derived_3(void)
{
    std::cout << "class_derived_3 DTOR" << std::endl;
}

void class_derived_3::do_work(void)
{
    std::cout << "class_derived_3 do_work()" << std::endl;
    help_3();
}

void class_derived_3::help_3(void)
{
    std::cout << "class_derived_3 help_3() - private method" << std::endl;
}
