
#include "class_base.h"

class class_derived_1: public class_base
{
public:
    class_derived_1(void);
    virtual ~class_derived_1(void);

protected:
    virtual void do_work(void);

    void help_1(void);
};
