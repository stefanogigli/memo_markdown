
#include "class_derived_1.h"
#include <iostream>

class_derived_1::class_derived_1(void): class_base()
{
    std::cout << "class_derived_1 CTOR" << std::endl;
}

class_derived_1::~class_derived_1(void)
{
    std::cout << "class_derived_1 DTOR" << std::endl;
}

void class_derived_1::do_work(void)
{
    std::cout << "class_derived_1 do_work()" << std::endl;
    help_1();
}

void class_derived_1::help_1(void)
{
    std::cout << "class_derived_1 help_1() - private method" << std::endl;
}
