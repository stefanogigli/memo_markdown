
#include "class_base.h"

#include <iostream>

class_base::class_base(void)
{
    std::cout << "class_base CTOR" << std::endl;
    init();
}
class_base::class_base(void)
{
    std::cout << "class_base DTOR" << std::endl;
    deinit();
}

void class_base::init(void)
{
    std::cout << "class_base init" << std::endl;
}

void class_base::deinit(void)
{
    std::cout << "class_base deinit" << std::endl;
    stop();
}


void class_base::start(int n)
{
    std::cout << "class_base start" << std::endl;
    int i;
    for(i = 0 ; i < n; ++i)
    {
        do_work();
    }
}

void class_base::stop(void)
{
    std::cout << "class_base stop" << std::endl;
}


void class_base::do_work(void)
{
    std::cout << "class_base do_work" << std::endl;
}

