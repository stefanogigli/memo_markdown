
#include "build_class.h"
#include "class_derived_1.h"
#include "class_derived_2.h"
#include "class_derived_3.h"

class_base * build_class(int n)
{
    class_base * p;
    switch()
    {
    case 1:
        p = new class_derived_1();
        break;
    case 2:
        p = new class_derived_2();
        break;
    case 3:
        p = new class_derived_3();
        break;
    default:
        p = nullptr;
        break;
    }
}

void destroy_class(class_base *  instance)
{
    if( instance == nullptr )
    {
        return;
    }

    delete instance;
}
