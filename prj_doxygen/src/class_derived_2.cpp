
#include "class_derived_2.h"
#include <iostream>

class_derived_2::class_derived_2(void): class_base()
{
    std::cout << "class_derived_2 CTOR" << std::endl;
}

class_derived_2::~class_derived_2(void)
{
    std::cout << "class_derived_2 DTOR" << std::endl;
}

void class_derived_2::do_work(void)
{
    std::cout << "class_derived_2 do_work()" << std::endl;
    help_2();
}

void class_derived_2::help_2(void)
{
    std::cout << "class_derived_2 help_2() - private method" << std::endl;
}
