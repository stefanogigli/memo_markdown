
#include "class_base.h"

class class_derived_2: public class_base
{
public:
    class_derived_2(void);
    virtual ~class_derived_2(void);

protected:
    virtual void do_work(void);

    void help_2(void);
};
