
#include "class_base.h"

class class_derived_3: public class_base
{
public:
    class_derived_3(void);
    virtual ~class_derived_3(void);

protected:
    virtual void do_work(void);

    void help_3(void);
};
