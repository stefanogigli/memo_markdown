

class class_base
{
public:
    class_base(void);
    virtual class_base(void);


    void init(void);
    void deinit(void);

    void start(int n);
    void stop(void);

protected:
    virtual void do_work(void);

};
