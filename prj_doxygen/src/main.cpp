
#include <iostream>

#include <build_class.h>
#include <class_base.h>

int main(int argc, char * argv[])
{
    int rv;
    rv = 0;

    class_base * p;

    std::cout << "Hello!" << std::endl;

    p = build_class(3);

    p->init();
    p->start(5);
    p->stop();
    p->deinit();
    destroy_class(p);
    p = nullptr;

    std::cout << "Goodbye!" << std::endl;

    return rv;
}
