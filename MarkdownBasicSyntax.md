# Markdown Basic Syntax


References:

- https://daringfireball.net/projects/markdown/basics
- https://daringfireball.net/projects/markdown/syntax
- https://www.ionos.com/digitalguide/websites/web-development/markdown/


--- 

# Titles and paragraph

# A First Level Header
Some Text as Paragraph.
```markdown
# A First Level Header
Some Text as Paragraph.
```

## A Second Level Header
Some Text as Paragraph.
```markdown
## A Second Level Header
Some Text as Paragraph.
```

### Header 3
Some Text as Paragraph.
```markdown
### Header 3
Some Text as Paragraph.
```

#### Header 4
Some Text as Paragraph.
```markdown
#### Header 4
Some Text as Paragraph.
```

##### Header 5
Some Text as Paragraph.
```markdown
##### Header 5
Some Text as Paragraph.
```

###### Header 6
Some Text as Paragraph.
```markdown
###### Header 6
Some Text as Paragraph.
```

---

# Blockquote

> This is a blockquote with two paragraphs. Lorem ipsum dolor sit amet,
> consectetuer adipiscing elit. Aliquam hendrerit mi posuere lectus.
> Vestibulum enim wisi, viverra nec, fringilla in, laoreet vitae, risus.
> 
> Donec sit amet nisl. Aliquam semper ipsum sit amet velit. Suspendisse
> id sem consectetuer libero luctus adipiscing.


```markdown
> This is a blockquote with two paragraphs. Lorem ipsum dolor sit amet,
> consectetuer adipiscing elit. Aliquam hendrerit mi posuere lectus.
> Vestibulum enim wisi, viverra nec, fringilla in, laoreet vitae, risus.
> 
> Donec sit amet nisl. Aliquam semper ipsum sit amet velit. Suspendisse
> id sem consectetuer libero luctus adipiscing.
```


> This is a blockquote.
>
> This is the second paragraph in the blockquote.
>
> ## This is an H2 in a blockquote
> 1.   This is the first list item.
> 2.   This is the second list item.
>
>     return shell_exec("echo $input | $markdown_script and take care about the 4 spaces added");

```markdown
> This is a blockquote.
>
> This is the second paragraph in the blockquote.
>
> ## This is an H2 in a blockquote
> 1.   This is the first list item.
> 2.   This is the second list item.
>
>     return shell_exec("echo $input | $markdown_script and take care about the 4 spaces added");
```

---

# Bold, Italic, Underline (not exist), strike, ...

Some of these words *are emphasized*.
Some of these words _are emphasized also_.

```markdown
Some of these words *are emphasized*.  
Some of these words _are emphasized also_.
```

Use two asterisks for **strong emphasis**.
Or, if you prefer, __use two underscores instead__.


```markdown
Use two asterisks for **strong emphasis**.  
Or, if you prefer, __use two underscores instead__.
```

***Italic and Bold Text***
___Italic and Bold Text___

```markdown
***Italic and Bold Text***
___Italic and Bold Text___
```

~~This text is struckthrough.~~

```
~~This text is struckthrough.~~
```

---

# Paragraphs


The Markdown language works with hard line breaks to separate paragraphs from each other. To create a completely new block of text (

tag), simply add an empty line. Important note: for Markdown, it’s sufficient if the line is visually empty. So if the line contains white spaces like tabs or spaces, the parser will ignore them and consider the line to be empty. If you want to create a line break like the
tag, add two spaces at the end of a line.

```

The Markdown language works with hard line breaks to separate paragraphs from each other. To create a completely new block of text (

tag), simply add an empty line. Important note: for Markdown, it’s sufficient if the line is visually empty. So if the line contains white spaces like tabs or spaces, the parser will ignore them and consider the line to be empty. If you want to create a line break like the
tag, add two spaces at the end of a line.

```


---

# Lists

List Type 1 (bullet/asterisk):

*   Candy.
*   Gum.
*   Booze.

```markdown
*   Candy.
*   Gum.
*   Booze.
```

List Type 2 (bullet/plus):

+   Candy.
+   Gum.
+   Booze.

```markdown
+   Candy.
+   Gum.
+   Booze.
```

List Type 3 (bullet/dash):

-   Candy.
-   Gum.
-   Booze.

```markdown
-   Candy.
-   Gum.
-   Booze.
```

List Type 4 (number/digits-point):

1.   Candy.
1.   Gum.
1.   Booze.

```markdown
1.   Candy.
1.   Gum.
1.   Booze.
```

List Type 5 (Checklist):

- [ ] Mercury
- [x] Venus
- [x] Earth (Orbit/Moon)
- [x] Mars


```markdown
- [ ] Mercury
- [x] Venus
- [x] Earth (Orbit/Moon)
- [x] Mars
```


Variant 1 (multiple paragraph with empty line): 

If you put blank lines between items, you’ll get <p> tags for the list item text. You can create multi-paragraph list items by indenting the paragraphs by 4 spaces or 1 tab:

*   A list item.

    With multiple paragraphs.

*   Another item in the list.

```markdown
*   A list item.

    With multiple paragraphs.

*   Another item in the list.
```

---

# Links, References and Images

## Links

This a first link example: <http://example.com/>


```markdown
This a first link example: <http://example.com/>
```

This is an [example link](http://example.com/).

```markdown
This is an [example link](http://example.com/).
```

This is an [example link](http://example.com/ "With a Title"). A Title should appear.

```markdown
This is an [example link](http://example.com/ "With a Title"). A Title should appear.
```

---

# Tables

|Column 1|Column 2|
|--------|--------|
|    A   |    B   |
|    C   |    D   |

```markdown
|Column 1|Column 2|
|--------|--------|
|    A   |    B   |
|    C   |    D   |
```


---

# References

## References with numbers: 

I get 10 times more traffic from [Google][1] than from [Yahoo][2] or [MSN][3].

[1]: http://google.com/        "Google"
[2]: http://search.yahoo.com/  "Yahoo Search"
[3]: http://search.msn.com/    "MSN Search"


```markdown
I get 10 times more traffic from [Google][1] than from [Yahoo][2] or [MSN][3].

[1]: http://google.com/        "Google"
[2]: http://search.yahoo.com/  "Yahoo Search"
[3]: http://search.msn.com/    "MSN Search"
```

---

## References with labels: 

I start my morning with a cup of coffee and [The New York Times][NY Times].

[NY times]: http://www.nytimes.com/

```markdown
I start my morning with a cup of coffee and [The New York Times][NY Times].

[NY times]: http://www.nytimes.com/
```

# Footnotes

You can easily place footnotes [^2] in the continuous text [^1].
[^1]: Here you can find the text for the footnote.
[^2]: **Footnotes** themselves can also be *formatted*.
And these even include several lines.

```
You can easily place footnotes [^2] in the continuous text [^1].
[^1]: Here you can find the text for the footnote.
[^2]: **Footnotes** themselves can also be *formatted*.
And these even include several lines.
```

---

### Images


### Image with inline style

![alt text](./resources/image_01_small.jpg "Title")

```markdown
![alt text](./resources/image_01_small.jpg "Title")
```

### Remote image

![alt text](https://www.linux.org/images/logo.png "Title for remote image")
```markdown
![alt text](https://www.linux.org/images/logo.png "Title for remote image")
```

### Image with reference style

**Seems does NOT work!**

![alt text][id]
[id]: ./resources/image_01_small.jpg "Title"

```markdown
![alt text][id]
[id]: ./resources/image_01_small.jpg "Title"
```


### Image and Link Together

[![Alternate Text](./resources/image_01_small.jpg 'Hover Text')](./resources/image_01.jpg)


```markdown
[![Alternate Text](./resources/image_01_small.jpg 'Hover Text')](./resources/image_01.jpg)
```

---

# Code

I strongly recommend against using any `<blink>` tags.
instead of decimal-encoded entities like `&#8212;`.
```
``There is a literal backtick (`) here.``
```markdown
``There is a literal backtick (`) here.``
```


Some JSON code with inline syntax highlight:

```json
{
    "property_1": "value1",
    "property_2": "value2",
    "property_3": 3,
    "property_4": true
}
```

Some C++ code with syntax highlight

```c++
{
    explicit myclass(void);
    virtual ~myclass(void);

    init(void);
    virtual exec(void);
    deinit(void);
};
```


Some python statements here.
```python
def function():
    a = 12
    b = 13
    return a + b

# a python comment here
```

Some shell statements here.
```shell
ls -l
mkdir pippo
cd pippo
```

---

## Escape

\*literal asterisks\*

```markdown
\*literal asterisks\*
```

Markdown provides backslash escapes for the following characters:
```markdown
\   backslash
`   backtick
*   asterisk
_   underscore
{}  curly braces
[]  square brackets
()  parentheses
#   hash mark
+   plus sign
-   minus sign (hyphen)
.   dot
!   exclamation mark
```

## Horizontal Rules

Use here three dashes

```
---
```
---

Use here three asterisks


```
***
```

***


## Separators and New lines 

Take care of some beaviour:

- each sequence of space is `1` separator (Space).  
- a single new line is `1` separator (Space).  
- a double new line is `1` new paragraph.
- more than double new line is `1` new paragraph.
- double space at the end of line + a single new-line is:  `1` new line but *in same paragraph*.



# Resources

https://www.markdownguide.org/getting-started/


Riferimenti per sintassi
- https://daringfireball.net/projects/markdown/syntax
    - https://daringfireball.net/projects/markdown/
    - https://daringfireball.net/projects/markdown/basics
    -
- https://www.markdownguide.org/basic-syntax/
    - https://www.markdownguide.org/hacks/
    - https://www.markdownguide.org/extended-syntax/
- https://docs.github.com/en/get-started/writing-on-github/getting-started-with-writing-and-formatting-on-github/basic-writing-and-formatting-syntax
- https://docs.github.com/en/get-started/writing-on-github/working-with-advanced-formatting/creating-and-highlighting-code-blocks
